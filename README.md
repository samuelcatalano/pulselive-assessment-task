# Pulselive Assessment Task
Pulselive - Java Developer Programming Test

##### Steps to run this code:
* You can clone this repository, download the zipfile or use the file sent by email
* Open your favourite IDE (**Eclipse IDE**, **IntelliJ** or **NetBeans**)
* Select File > Import > Existing Project into Workspace
* I created a class called **PulseliveFootballTable** just to check/print a few results
* Auxiliary classes created:
  * **Team.java**
  * **TableEntryComparator.java**
  * **GoalsComparator.java**
  * **GoalsForComparator.java**
  * **NameComparator.java**
  * **PointsComparator.java**
  * **InvalidGoalValueException.java**
  * **PulseliveFootballTable.java**
  * All code are here and available for check


![image](https://drive.google.com/uc?export=view&id=1LDWxAMZcqnEj4NGn_fyqn6rGV78uOnQ3)



Thank you so much for the opportunity

:soccer: 