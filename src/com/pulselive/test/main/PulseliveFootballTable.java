package com.pulselive.test.main;

import com.pulselive.test.base.LeagueTable;
import com.pulselive.test.base.LeagueTableEntry;
import com.pulselive.test.base.Match;
import com.pulselive.test.exception.InvalidGoalValueException;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Samuel Catalano
 * @since 2019-08-10
 */
public class PulseliveFootballTable {

	private static final Logger LOG = Logger.getLogger(PulseliveFootballTable.class.getName());

	public static void main(final String... args) {

									//		 2018-19 - TEAMS
		                            //       The league will have 38 rounds where all teams play at home and away.
		
									//		 Arsenal
									//		 Bournemouth
									//		 Brighton & Hove Albion
									//		 Burnley
									//		 Cardiff
									//		 Chelsea
									//		 Crystal Palace
									//		 Everton
									//		 Fulham
									//		 Huddersfield Town
									//		 Leicester City
									//		 Liverpool
									//		 Manchester City
									//		 Manchester United
									//		 Newcastle United
									//		 Southampton
									//		 Tottenham 
									//		 Watford	
									//		 West Ham	
									//		 Wolverhampton

		final LeagueTable table;

		// A few matches just to test and out of order to sort by criteria
		final Match match1  = new Match("Arsenal", "Liverpool", 5, 1);
		final Match match5  = new Match("Arsenal", "Chelsea", 0, 2);
		final Match match11 = new Match("Arsenal", "Tottenham", 1, 0);
		final Match match20 = new Match("Arsenal", "Fulham", 2, 2);
		
		final Match match2  = new Match("Liverpool", "Arsenal", 3, 3);
		final Match match7  = new Match("Liverpool", "Tottenham", 3, 0);
		final Match match9  = new Match("Liverpool", "Chelsea", 1, 1);
		final Match match14 = new Match("Liverpool", "Fulham", 1, 2);
		
		final Match match3  = new Match("Chelsea", "Tottenham", 1, 4);
		final Match match6  = new Match("Chelsea", "Arsenal", 1, 0);
		final Match match17 = new Match("Chelsea", "Liverpool", 2, 2);
		final Match match15 = new Match("Chelsea", "Fulham", 4, 2);

		final Match match4  = new Match("Tottenham", "Chelsea", 1, 0);
		final Match match8  = new Match("Tottenham", "Liverpool", 3, 3);
		final Match match12 = new Match("Tottenham", "Arsenal", 2, 4);
		final Match match16 = new Match("Tottenham", "Fulham", 2, 3);
		
		final Match match10 = new Match("Fulham", "Chelsea", 5, 1);
		final Match match18 = new Match("Fulham", "Liverpool", 4, 3);
		final Match match19 = new Match("Fulham", "Arsenal", 1, 5);
		final Match match13 = new Match("Fulham", "Tottenham", 3, 6);
		
		// List of Matches
		final List<Match> matches = new ArrayList<>( );

		matches.add(match1);
		matches.add(match2);
		matches.add(match3);
		matches.add(match4);
		matches.add(match5);
		matches.add(match6);
		matches.add(match7);
		matches.add(match8);
		matches.add(match9);
		matches.add(match10);
		matches.add(match11);
		matches.add(match12);
		matches.add(match13);
		matches.add(match14);
		matches.add(match15);
		matches.add(match16);
		matches.add(match17);
		matches.add(match18);
		matches.add(match19);
		matches.add(match20);

		try {
			table = new LeagueTable(matches);
			final List<LeagueTableEntry> list = table.getTableEntries();

			// Printing
			System.out.format("\n %80s", "===========================================================================================");
			System.out.format("\n %7s%14s%11s%8s%11s%11s%8s%10s%10s", "TEAM", "GAMES", "POINTS", "WINS", "DRAWS", "LOSSES", "GF", "GA", "GD");
			System.out.format("\n %80s", "===========================================================================================");
			System.out.println();

			for (final LeagueTableEntry league : list) {
				System.out.format("%10s%10s%10s%10s%10s%10s%11s%10s%10s",
						league.getTeamName( ),
						league.getPlayed(),
						league.getPoints(),
						league.getWon(),
						league.getDrawn(),
						league.getLost(),
						league.getGoalsFor(),
						league.getGoalsAgainst(),
						league.getGoalDifference());
				System.out.println();
			}
		} catch (final InvalidGoalValueException e) {
			LOG.log(Level.SEVERE, "Check you score inputs! Score cannot be a negative value ", e);
		}
	}
}