package com.pulselive.test.utils;

import java.util.Comparator;

import com.pulselive.test.base.LeagueTableEntry;

/**
 * @author Samuel Catalano
 * @since 2019-08-10
 */
public class GoalsComparator implements Comparator<LeagueTableEntry> {

	@Override
	public int compare(final LeagueTableEntry leagueTableEntry1, final LeagueTableEntry leagueTableEntry2) {
		if (leagueTableEntry1.getGoalDifference() == leagueTableEntry2.getGoalDifference())
			return 0;
		return leagueTableEntry1.getGoalDifference() > leagueTableEntry2.getGoalDifference() ? -1 : 1;
	}
}