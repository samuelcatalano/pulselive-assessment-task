package com.pulselive.test.utils;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import com.pulselive.test.base.LeagueTableEntry;

/**
 * @author Samuel Catalano
 * @since 2019-08-10
 */
public class TableEntryComparator implements Comparator<LeagueTableEntry> {

	private List<Comparator<LeagueTableEntry>> listComparators;

	@SafeVarargs
	public TableEntryComparator(Comparator<LeagueTableEntry>... comparators) {
		this.listComparators = Arrays.asList(comparators);
	}

	@Override
	public int compare(LeagueTableEntry leagueTableEntry1, LeagueTableEntry leagueTableEntry2) {
		for (Comparator<LeagueTableEntry> comparator : listComparators) {
			int result = comparator.compare(leagueTableEntry1, leagueTableEntry2);
			if (result != 0) {
				return result;
			}
		}
		return 0;
	}
}