package com.pulselive.test.utils;

import java.util.Comparator;

import com.pulselive.test.base.LeagueTableEntry;

/**
 * @author Samuel Catalano
 * @since 2019-08-10
 */
public class PointsComparator implements Comparator<LeagueTableEntry> {

	@Override
	public int compare(LeagueTableEntry leagueTableEntry1, LeagueTableEntry leagueTableEntry2) {
		if (leagueTableEntry1.getPoints() == leagueTableEntry2.getPoints())
			return 0;
		return leagueTableEntry1.getPoints() > leagueTableEntry2.getPoints() ? -1 : 1;
	}
}