package com.pulselive.test.utils;

import java.util.Comparator;

import com.pulselive.test.base.LeagueTableEntry;

/**
 * @author Samuel Catalano
 * @since 2019-08-10
 */
public class NameComparator implements Comparator<LeagueTableEntry> {

	@Override
	public int compare(LeagueTableEntry leagueTableEntry1, LeagueTableEntry leagueTableEntry2) {
		return leagueTableEntry1.getTeamName().compareTo(leagueTableEntry2.getTeamName());
	}
}