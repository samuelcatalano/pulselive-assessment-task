package com.pulselive.test.exception;

/**
 * @author Samuel Catalano
 * @since 2019-08-10
 */
public class InvalidGoalValueException extends Exception {

    public InvalidGoalValueException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public InvalidGoalValueException(final String message) {
        super(message);
    }

    public InvalidGoalValueException(final Throwable cause) {
        super(cause);
    }

    public InvalidGoalValueException() {
        super();
    }

}
