package com.pulselive.test.base;

/**
 * @author Samuel Catalano
 * @since 2019-08-10
 */
public class Team {

	private String name;
	private int numberOfMatches;
	private int numberOfVictories;
	private int numberOfLosses;
	private int numberOfDraws;
	private int goalsFor;
	private int goalsAway;
	private int totalPoints;

	public Team(String name, int numberOfMatches, int numberOfVictories, int numberOfLosses, int numberOfDraws, int goalsFor, int goalsAway, int totalPoints) {
		this.name = name;
		this.numberOfMatches = numberOfMatches;
		this.numberOfVictories = numberOfVictories;
		this.numberOfLosses = numberOfLosses;
		this.numberOfDraws = numberOfDraws;
		this.goalsFor = goalsFor;
		this.goalsAway = goalsAway;
		this.totalPoints = totalPoints;
	}

	public String getName( ) {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNumberOfMatches( ) {
		return numberOfMatches;
	}

	public void setNumberOfMatches(int numberOfMatches) {
		this.numberOfMatches = numberOfMatches;
	}

	public int getNumberOfVictories( ) {
		return numberOfVictories;
	}

	public void setNumberOfVictories(int numberOfVictories) {
		this.numberOfVictories = numberOfVictories;
	}

	public int getNumberOfLosses( ) {
		return numberOfLosses;
	}

	public void setNumberOfLosses(int numberOfLosses) {
		this.numberOfLosses = numberOfLosses;
	}

	public int getNumberOfDraws( ) {
		return numberOfDraws;
	}

	public void setNumberOfDraws(int numberOfDraws) {
		this.numberOfDraws = numberOfDraws;
	}

	public int getGoalsFor( ) {
		return goalsFor;
	}

	public void setGoalsFor(int goalsFor) {
		this.goalsFor = goalsFor;
	}

	public int getGoalsAway( ) {
		return goalsAway;
	}

	public void setGoalsAway(int goalsAway) {
		this.goalsAway = goalsAway;
	}

	public int getTotalPoints( ) {
		return totalPoints;
	}

	public void setTotalPoints(int totalPoints) {
		this.totalPoints = totalPoints;
	}

	public int getGoalsDifference( ) {
		return (this.getGoalsFor( ) - this.getGoalsAway( ));
	}
}