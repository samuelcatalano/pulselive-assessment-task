package com.pulselive.test.base;

import com.pulselive.test.exception.InvalidGoalValueException;
import com.pulselive.test.utils.*;

import java.util.*;

/**
 * @author Samuel Catalano
 * @since 2019-08-10
 */
public class LeagueTable {
	
	private final Set<LeagueTableEntry> entries = new HashSet<LeagueTableEntry>( );

	/**
	 * Create a league table from the supplied list of match results
	 * @param matches
	 */
	public LeagueTable(final List<Match> matches) throws InvalidGoalValueException {
		for (final Match match : matches) {
			if (!this.checkIfTeamAlreadyExist(match.getHomeTeam( ))) {
				Team team = null;
				LeagueTableEntry entry = null;

				if (match.getHomeScore() < 0 || match.getAwayScore() < 0) {
					throw new InvalidGoalValueException("The score cannot be a negative value");
				} else {
					if (match.isHomeTeamTheWinner( ) == Boolean.TRUE) {
						team = new Team(match.getHomeTeam( ),
								1,
								1,
								0,
								0,
								match.getHomeScore( ),
								match.getAwayScore( ),
								3); // winner 3 points
					}
					else if (match.isHomeTeamTheWinner( ) == Boolean.FALSE) {
						team = new Team(match.getHomeTeam( ),
								1,
								0,
								1,
								0,
								match.getAwayScore( ),
								match.getHomeScore( ),
								0); // winner 0 points
					}
					else if (match.isHomeTeamTheWinner( ) == null) { // draw
						team = new Team(match.getHomeTeam( ),
								1,
								0,
								0,
								1,
								match.getHomeScore( ),
								match.getAwayScore( ),
								1); // winner 1 points
					}

					// new League Table Entry
					entry = new LeagueTableEntry(
							team.getName( ),
							team.getNumberOfMatches( ),
							team.getNumberOfVictories( ),
							team.getNumberOfDraws( ),
							team.getNumberOfLosses( ),
							team.getGoalsFor( ),
							team.getGoalsAway( ),
							team.getGoalsDifference( ),
							team.getTotalPoints( )
					);

					this.entries.add(entry);
				}
			}
			else {
				if (match.getHomeScore() < 0 || match.getAwayScore() < 0) {
					throw new InvalidGoalValueException("The score cannot be a negative value");
				} else {
					final LeagueTableEntry entry = this.getSpecificLeagueEntryByTeamName(match.getHomeTeam( ));

					if (match.isHomeTeamTheWinner( ) == Boolean.TRUE) {
						entry.setPoints(entry.getPoints( ) + 3);
						entry.setPlayed(entry.getPlayed( ) + 1);
						entry.setWon(entry.getWon( ) + 1);
						entry.setGoalsFor(entry.getGoalsFor( ) + match.getHomeScore( ));
						entry.setGoalsAgainst(entry.getGoalsAgainst( ) + match.getAwayScore( ));
						entry.setGoalDifference(entry.getGoalDifference( ) + (match.getHomeScore( ) - match.getAwayScore( )));
					}
					else if (match.isHomeTeamTheWinner( ) == Boolean.FALSE) {
						entry.setPlayed(entry.getPlayed( ) + 1);
						entry.setLost(entry.getLost( ) + 1);
						entry.setGoalsFor(entry.getGoalsFor( ) + match.getHomeScore( ));
						entry.setGoalsAgainst(entry.getGoalsAgainst( ) + match.getAwayScore( ));
						entry.setGoalDifference(entry.getGoalDifference( ) + (match.getHomeScore( ) - match.getAwayScore( )));
					}
					else if (match.isHomeTeamTheWinner( ) == null) {
						entry.setPoints(entry.getPoints( ) + 1);
						entry.setPlayed(entry.getPlayed( ) + 1);
						entry.setDrawn(entry.getDrawn( ) + 1);
						entry.setGoalsFor(entry.getGoalsFor( ) + match.getHomeScore( ));
						entry.setGoalsAgainst(entry.getGoalsAgainst( ) + match.getAwayScore( ));
						entry.setGoalDifference(entry.getGoalDifference( ) + (match.getHomeScore( ) - match.getAwayScore( )));
					}

					this.entries.add(entry);
				}
			}
		} 
	}
	
	/**
	 * Get the ordered list of league table entries for this league table.
	 * @return entries sorted by points, goal difference, goals for and then team names
	 */
	public List<LeagueTableEntry> getTableEntries( ) {
		final List<LeagueTableEntry> list = new ArrayList<>(this.entries);
		Collections.sort(list, new TableEntryComparator(
				new PointsComparator( ),
				new GoalsComparator( ),
				new GoalsForComparator( ),
				new NameComparator( )));

		return list;
	}
	
	/**
	 * getSpecificLeagueEntryByTeamName
	 * @param string
	 * @return {@link LeagueTableEntry}
	 */
	private LeagueTableEntry getSpecificLeagueEntryByTeamName(final String string) {
		final List<LeagueTableEntry> entries = this.getTableEntries( );
		for (final LeagueTableEntry entry : entries) {
			if (entry.getTeamName( ).equalsIgnoreCase(string)) {
				return entry;
			}
		}
		return null;
	}
	
	/**
	 * @param name
	 * @return <code>true</code> or <code>false</code>
	 */
	private boolean checkIfTeamAlreadyExist(final String name) {
		final List<LeagueTableEntry> list = new ArrayList<>(this.entries);
		if (list != null) {
			for (int i = 0; i < list.size( ); i++) {
				if (list.get(i).getTeamName( ).equalsIgnoreCase(name)) {
					return true;
				}
			}
		}
		return false;
	}
}