package com.pulselive.test.base;

/**
 * @author Samuel Catalano
 * @since 2019-08-10
 */
public class LeagueTableEntry {

	private String teamName;
	private int played = 0;
	private int won = 0;
	private int drawn = 0;
	private int lost = 0;
	private int goalsFor = 0;
	private int goalsAgainst = 0;
	private int goalDifference = 0;
	private int points = 0;
	
	/**
	 * Constructor
	 * @param teamName
	 * @param played
	 * @param won
	 * @param drawn
	 * @param lost
	 * @param goalsFor
	 * @param goalsAgainst
	 * @param goalDifference
	 * @param points
	 */
	public LeagueTableEntry(final String teamName, final int played, final int won, final int drawn, final int lost, final int goalsFor, final int goalsAgainst, final int goalDifference, final int points) {
		this.teamName = teamName;
		this.played = played;
		this.won = won;
		this.drawn = drawn;
		this.lost = lost;
		this.goalsFor = goalsFor;
		this.goalsAgainst = goalsAgainst;
		this.goalDifference = goalDifference;
		this.points = points;
	}

	/**
	 * @return the teamName
	 */
	public String getTeamName() {
		return this.teamName;
	}

	/**
	 * @return the played
	 */
	public int getPlayed() {
		return this.played;
	}

	/**
	 * @return the won
	 */
	public int getWon() {
		return this.won;
	}

	/**
	 * @return the drawn
	 */
	public int getDrawn() {
		return this.drawn;
	}

	/**
	 * @return the lost
	 */
	public int getLost() {
		return this.lost;
	}

	/**
	 * @return the goalsFor
	 */
	public int getGoalsFor() {
		return this.goalsFor;
	}

	/**
	 * @return the goalsAgainst
	 */
	public int getGoalsAgainst() {
		return this.goalsAgainst;
	}

	/**
	 * @return the goalDifference
	 */
	public int getGoalDifference() {
		return this.goalDifference;
	}

	/**
	 * @return the points
	 */
	public int getPoints() {
		return this.points;
	}

	/**
	 * @param teamName the teamName to set
	 */
	public void setTeamName(final String teamName) {
		this.teamName = teamName;
	}

	/**
	 * @param played the played to set
	 */
	public void setPlayed(final int played) {
		this.played = played;
	}

	/**
	 * @param won the won to set
	 */
	public void setWon(final int won) {
		this.won = won;
	}

	/**
	 * @param drawn the drawn to set
	 */
	public void setDrawn(final int drawn) {
		this.drawn = drawn;
	}

	/**
	 * @param lost the lost to set
	 */
	public void setLost(final int lost) {
		this.lost = lost;
	}

	/**
	 * @param goalsFor the goalsFor to set
	 */
	public void setGoalsFor(final int goalsFor) {
		this.goalsFor = goalsFor;
	}

	/**
	 * @param goalsAgainst the goalsAgainst to set
	 */
	public void setGoalsAgainst(final int goalsAgainst) {
		this.goalsAgainst = goalsAgainst;
	}

	/**
	 * @param goalDifference the goalDifference to set
	 */
	public void setGoalDifference(final int goalDifference) {
		this.goalDifference = goalDifference;
	}

	/**
	 * @param points the points to set
	 */
	public void setPoints(final int points) {
		this.points = points;
	}
}