package com.pulselive.test.base;

/**
 * @author Samuel Catalano
 * @since 2019-08-10
 */
public class Match {

	private String homeTeam;
	private String awayTeam;
	private int homeScore;
	private int awayScore;
	
	public Match(final String homeTeam, final String awayTeam, final int homeScore, final int awayScore) {
		this.homeTeam = homeTeam;
		this.awayTeam = awayTeam;
		this.homeScore = homeScore;
		this.awayScore = awayScore;
	}
	
	public Boolean isHomeTeamTheWinner( ) {
		Boolean winner = null;

		if (homeScore > awayScore) {
			winner = Boolean.TRUE;
		}
		else if (awayScore > homeScore) {
			winner = Boolean.FALSE;
		}
		else if (homeScore == awayScore) {
			winner = null;
		}
		return winner;
	}
	
	/**
	 * @return the homeTeam
	 */
	public String getHomeTeam() {
		return homeTeam;
	}

	/**
	 * @return the awayTeam
	 */
	public String getAwayTeam() {
		return awayTeam;
	}

	/**
	 * @return the homeScore
	 */
	public int getHomeScore() {
		return homeScore;
	}

	/**
	 * @return the awayScore
	 */
	public int getAwayScore() {
		return awayScore;
	}

	/**
	 * @param homeTeam the homeTeam to set
	 */
	public void setHomeTeam(String homeTeam) {
		this.homeTeam = homeTeam;
	}

	/**
	 * @param awayTeam the awayTeam to set
	 */
	public void setAwayTeam(String awayTeam) {
		this.awayTeam = awayTeam;
	}

	/**
	 * @param homeScore the homeScore to set
	 */
	public void setHomeScore(int homeScore) {
		this.homeScore = homeScore;
	}

	/**
	 * @param awayScore the awayScore to set
	 */
	public void setAwayScore(int awayScore) {
		this.awayScore = awayScore;
	}
}